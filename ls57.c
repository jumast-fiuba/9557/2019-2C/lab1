#define _ATFILE_SOURCE
#define _DEFAULT_SOURCE
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include "string.h"
#include <errno.h>
#include <sys/statfs.h>

#define PARAMS 2
#define PARAM_DIRECTORIO 1

int main(int argc, char* argv[]){

    if(argc > PARAMS){
        fprintf(stderr, "Error: se esperaban 0 o 1 argumentos y se obtuvieron %i\n", argc);
    }

    char* dir_name = NULL;
    if(argc == 1){
        dir_name = ".";
    }
    else{
        dir_name = argv[PARAM_DIRECTORIO];
    }

    errno = 0;
    DIR* dir_ptr = opendir(dir_name);
    if(dir_ptr == NULL){
        if(errno == ENOTDIR){
            fprintf(stderr, "Solo se pueden listar directorios\n");
        }
        else{
            perror("Error");
        }
        return EXIT_FAILURE;
    }

    int dir_fd = dirfd(dir_ptr);
    if(dir_fd == -1){
        closedir(dir_ptr);
        perror("Error");
        return EXIT_FAILURE;
    }

    struct dirent* dir_entry = NULL;
    errno = 0;
    while((dir_entry = readdir(dir_ptr)) != NULL){

        char* entry_name = dir_entry->d_name;

        // omitir archivos ocultos, el directorio actual (".") y el padre ("..")
        if(entry_name[0] == '.'){
            continue;
        }

        struct stat st;
        int stat_ret = fstatat(dir_fd, entry_name ,&st, 0);
        if(stat_ret == -1){
            closedir(dir_ptr);
            perror("Error");
            return EXIT_FAILURE;
        }

        if (S_ISREG(st.st_mode)){
            printf("%s\t%8ld\n", entry_name,st.st_size);
        }
        else{
            printf("%s\n", entry_name);
        }

        errno = 0;
    }

    closedir(dir_ptr);

    if(errno != 0){
        perror("Error");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}