#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <bits/stdint-uintn.h>
#include <errno.h>
#include <stdbool.h>
#define PERMISOS 0666


/*
 * argv[0]: Nombre del programa
 * argv[1]: Ruta del archivo origen
 * argv[2]: Ruta del arhivo destino
 */
#define PARAMS 3
#define ARCH_ORIG 1
#define ARCH_DEST 2
#define BUFSIZE 512

int main(int argc, char *argv[]) {

    if (argc != PARAMS) {
        fprintf(stderr, "Error: se esperaban %i argumentos y se recibieron %i\n", PARAMS - 1, argc - 1);
        return EXIT_FAILURE;
    }

    int fd_orig = 0;
    if ((fd_orig = open(argv[ARCH_ORIG], O_RDONLY)) == -1) {
        fprintf(stderr, "Error al abrir %s: %m\n", argv[ARCH_ORIG]);
        return EXIT_FAILURE;
    }

    int fd_dest = 0;
    if ((fd_dest = creat(argv[ARCH_DEST], PERMISOS)) == -1) {
        fprintf(stderr, "Error: no se puede abrir/crear %s: %m\n", argv[ARCH_DEST]);
        close(fd_orig);
        return EXIT_FAILURE;
    }

    uint8_t buf[BUFSIZE];
    int nbread = 0;
    errno = 0;
    while((nbread = read(fd_orig, buf, BUFSIZE)) > 0 || (nbread == -1 && errno == EINTR)){

        int nbremaining = nbread;
        int nbwritten = 0;

        do {
            errno = 0;
            nbwritten = write(fd_dest, buf, nbremaining - 4);
            if(nbwritten > 0){
                nbremaining -= nbwritten;
            }

        } while ((nbwritten > 0 && nbremaining > 0) || (nbwritten == -1 && errno == EINTR));

        errno = 0;
    }

    close(fd_orig);
    close(fd_dest);

    if (nbread == 0) {
        return EXIT_SUCCESS;
    }

    if (errno == EISDIR) {
        fprintf(stderr, "Error: el archivo %s es un directorio\n", argv[ARCH_ORIG]);
        return EXIT_FAILURE;
    }

    perror("Error");
    return EXIT_FAILURE;

}