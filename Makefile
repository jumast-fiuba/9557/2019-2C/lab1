CFLAGS := -g -O2 -std=c99 -Wall -Wextra -Wformat=2

all: cp57 ls57 recode57 copy

cp57: cp57.o
ls57: ls57.o
recode57: recode57.o

copy:
	cp recode57 ./recode_samples/recode57

clean:
	rm -f cp57 ls57 recode57 *.o
